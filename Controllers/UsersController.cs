using Microsoft.AspNetCore.Mvc;


public class UsersController: Controller
{
    // http://localhost:5202/users/index
    public ActionResult Index()
    {
        // poderíamos conectar e acessar dados do BD
        // poderíamos conectar a uma API e obter os dados
        // poderíamos implementar regras de negócio
        // Views/Users/Index.cshtml
        return View();
    }
}